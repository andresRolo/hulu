package com.hulu.dto;

public class PaintRequest{

	private String[][] screen;
	private int xValue;
	private int yValue;
	private String targetColor;

	public String[][] getScreen() {
		return screen;
	}

	public void setScreen(String[][] screen) {
		this.screen = screen;
	}

	public int getxValue() {
		return xValue;
	}

	public void setxValue(int xValue) {
		this.xValue = xValue;
	}

	public int getyValue() {
		return yValue;
	}

	public void setyValue(int yValue) {
		this.yValue = yValue;
	}

	public String getTargetColor() {
		return targetColor;
	}

	public void setTargetColor(String targetColor) {
		this.targetColor = targetColor;
	}
}
