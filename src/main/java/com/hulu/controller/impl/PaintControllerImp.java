package com.hulu.controller.impl;

import com.hulu.controller.PaintController;
import com.hulu.dto.PaintRequest;
import com.hulu.service.PaintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author arueda
 *
 */
@RestController
public class PaintControllerImp implements PaintController{

	@Autowired
	PaintService paintService;

	public String[][] paintFill(PaintRequest paintRequest){
		return paintService.paintFill(paintRequest);
	}

}
