package com.hulu.controller;

import com.hulu.dto.PaintRequest;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

/**
 * @author arueda
 *
 */
@ResponseBody
@RequestMapping(PaintController.PAINT)
@Api(value = PaintController.VALUE,
		tags = {PaintController.VERSION})
public interface PaintController {

    String VERSION = "/1.0";
    String PAINT = "/paint";
    String VALUE = "Paint Service";

	@PutMapping("/paintFill")
	@ApiOperation(
			value = "paint fill function updating the color of surrounding area"
    )
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "OK", response = String[][].class)
			}
	)
	String[][] paintFill(@RequestBody PaintRequest paintRequest);

}
