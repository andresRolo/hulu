package com.hulu.service;

import com.hulu.dto.PaintRequest;

/**
 * @author arueda
 *
 */
public interface PaintService {

	public String[][] paintFill(PaintRequest paintRequest);

}
