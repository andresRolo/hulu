package com.hulu.service.impl;

import com.hulu.dto.PaintRequest;
import com.hulu.service.PaintService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author arueda
 */
@RestController
public class PaintServiceImp implements PaintService {

    @Value("${matrix.size}")
    private int matrixSize;

    public String[][] paintFill(PaintRequest paintRequest) {
        if (!this.isValidRequest(paintRequest)) {
            return null;
        }
        String baseColor = this.getBaseColor(paintRequest);
        this.updateSurroundingArea(paintRequest, paintRequest.getxValue(), paintRequest.getyValue(), baseColor);
        return paintRequest.getScreen();
    }

    private Boolean isValidRequest(PaintRequest paintRequest) {
        boolean isXPointValid = this.isPointValid(paintRequest.getxValue());
        boolean isYPointValid = this.isPointValid(paintRequest.getyValue());
        boolean isColorValid = this.isColorValid(paintRequest.getTargetColor());
        //TODO Should validate if is required create boolean variables vs call directly the methods in return statement.
        return isXPointValid && isYPointValid && isColorValid;
    }

    private Boolean isPointValid(int point) {
        return point >= 0 && point < matrixSize;
    }

    private Boolean isColorValid(String color) {
        return color != null && !color.isEmpty();
    }

    private String getBaseColor(PaintRequest paintRequest) {
        return paintRequest.getScreen()[paintRequest.getxValue()][paintRequest.getyValue()];
    }

    private void updateSurroundingArea(PaintRequest paintRequest, int xPoint, int yPoint, String baseColor) {
        if (!this.isPointValid(xPoint) || !this.isPointValid(yPoint))
            return;
        if (!paintRequest.getScreen()[xPoint][yPoint].equals(baseColor))
            return;

        paintRequest.getScreen()[xPoint][yPoint] = paintRequest.getTargetColor();

        updateSurroundingArea(paintRequest, xPoint + 1, yPoint, baseColor);
        updateSurroundingArea(paintRequest, xPoint - 1, yPoint, baseColor);
        updateSurroundingArea(paintRequest, xPoint, yPoint + 1, baseColor);
        updateSurroundingArea(paintRequest, xPoint, yPoint - 1, baseColor);
    }

}
