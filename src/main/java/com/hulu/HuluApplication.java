package com.hulu;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class HuluApplication implements CommandLineRunner{

	protected final Log logger = LogFactory.getLog(getClass());

	public static void main(String[] args) {
		SpringApplication.run(HuluApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("HuluApplication Started !!");
	}
}
