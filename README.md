# hulu

Paint fill exercise for Hulu group in Globant

#Tools used

Spring + RestAPI + Swagger (IntelliJ IDEA tool)

#Test

1. git clone
2. mvn clean install
3. exec HuluApplication spring class
4. Access http://localhost:8082/swagger-ui.html#//1.0/paintFillUsingPUT URL
5. Demo data:
{
  "screen": [
    [
      "a","c","c","c","a"
    ],[
      "b","b","c","b","a"
    ],[
      "a","a","c","a","d"
    ],[
      "a","b","d","a","c"
    ],[
      "d","b","c","a","d"
    ]
  ],
  "targetColor": "x",
  "xValue": 4,
  "yValue": 3
}
